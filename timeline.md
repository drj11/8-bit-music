1977 Apple II, Atari VCS
1979 Intellivision, Atari 400
1981 BBC Model B, Mockingboard
1982 Commodore 64, ZX Spectrum, Atari VCS renamed Atari 2600
1983 Famicom (Japan), MSX computers such as Sony HB-55
1984 Amstrad CPC 464
1985 NES (US), Sega Master System
1986 128K ZX Spectrum, Atari 520STFM
1989 Game Boy (Japan)
1990 Super Famicom, Game Gear (Japan)
1991 SNES, Lagrange Point (VRC7 chip)
1998 Game Boy Color
1999 WonderSwan
2001 Game Boy Advance
