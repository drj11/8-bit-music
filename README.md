# 8-bit video game music

a six part podcast

1 Introduction
2 Development
3 Hardware
4 Technical
5 Composers
6 Genre and future


## Short jingles

Code Name: Viper (NES) Area Clear. https://www.youtube.com/watch?v=mQT-yay3K1w

Metroid (NES, 1986) item sound. https://www.youtube.com/watch?v=xpAB3kkIXg4

Duck Tales (NES, 1989) item sound. https://www.youtube.com/watch?v=m3pegQLo2-8

Megaman 2 (NES), Victory.

Probably something from Alien 3

Snafu (Intellivision) Game Over https://www.youtube.com/watch?v=ZiUkWLMC6z8

Gradius (NES) Game Over

Galaxians (Arcade) Game Start.

The Little Mermaid (NES) Game Over.

Chip 'n Dale Rescue Rangers (NES) Stage Clear.

Duck Tales (NES) Item Found.

Super Mario Bros (NES) Life Lost, Game Over 1, Level Clear.

Mario Golf (GBC) Course Intro

Batman Return of the Joker (GB) Game Over https://www.youtube.com/watch?v=2Pk9JBSrEgM

Pokemon Yellow (GB) Pokemon healed

Metal Gear (MSX) Dead Soldier https://www.youtube.com/watch?v=aVKRJhIyvhk

## Early History

Music on a PDP-1 with custom audio circuit
http://www.dpbsmith.com/pdp1music/index.html

Include Bach's Little Fugue: http://www.dpbsmith.com/pdp1music/littlefugue.mp3

## 1 Introduction

    [ Tetris (Game Boy) A-Type ]


Ah Tetris.
The game that for Nintendo relaunched its Game Boy's marketing campaign.
Tetris was the perfect pocket game and an inspired pairing for
Nintendo's portable console.

Anyone who has been exposed to Tetris will have been exposed to
its theme tune which is an earworm to inspire 1000 hours of gameplay.

Welcome to BeepCast a series of articles about the kind of
8-bit video game music that the Tetris theme epitomises.

I want to take you on a journey that I feel
lies at the heart of "computer music".

To avoid theorising about the origins of video game music,
I'm going to start with Space Invaders (1978).

    [ Space Invaders (Arcade, 1978) BGM ]

It's 4 note drone symbolising the start of the video game music movement
and the defeat of humanity by the Space Invaders.

As I make this article,
music in video games is practically unlimited in its scope and expression.
If you want to record hours of symphony orchestra
in high-fidelity and multi-channel, you can,
and you can feature it in a video game.

All those high production values are boring.

Let's talk about 8-bit music with a distinctive raw _computer_ sound.
That distinctive sound stems from the way the sound is made.
With chips.
The higher the fidelity of the sound reproduction,
the less the music sounds "computery", and the more it sounds, well,
like music.

The Sony Playstation had CD quality recorded sound
that could be streamed from the disc.
Effectively ending the era of
computer music in video games as a distinct sound.

We'll talk about tunes and music from the era of arcade games,
home computers before the PC era, and home consoles.
Names like Sega, Namco, Taito in the arcades, and
Nintendo, Commodore, Acorn at home. (and others)

    [ Donkey Kong (Arcade, 1981) Intro to action start ]

    First 48s of https://www.youtube.com/watch?v=Pp2aMs38ERY
    might be appropriate.

Something magical was happening.
Computer chips and televisions came together
and gave birth to video games.
Video games swept into the arcades and swept the midway clean.
Only moments later a second market opens up when
video game consoles are sold into our homes.

    [ Super Mario Bros (NES) Overworld ]

This is an exciting period in music.
It's a period in which computer programmers,
electronic engineers, and musicians are coming together to make
digital music.

We go from bleeps and bloops and short fanfare bursts to
real music.

    [ Princess Tomato in the Salad Kingdom (NES, 1988) ]

    [ Think I'm blowing the joke here, save for later? ]

We're going to hear music from a variety of home consoles,
and home computers.

That's hardware from

Atari

    [ Have you played Atari today? (jingle) ]
    [ Zybex (Atar XL, 1988) Game Over ]

Sinclair

    [ Manic Miner (ZX Spectrum, 1983) BGM ]

Commodore

    [ Uridium (C64, 1986) title ]
    [ The Great Giana Sisters ]

Sega

    [ Sega jingle? ]

and some slightly more obscure platforms.

But arching over everything, is the king of the 8-bit era.

    [ Super Mario Bros (NES) Castle Clear ]

Nintendo.

    Late to the party, Nintendo launched the Famicom in Japan in 1983,
    6 years after the Apple II computer and the Atari VCS
    console, and changed the face of video gaming history.

    [ Kirby's Adventure (NES, 1993) Ending Theme ]


The early music is very brief, often just a few simples notes
that make more of a jingle than a tune.

    [ Defender (Arcade, 1981) High Score ]

We get title music,

Background music,

    [ Kirby's Adventure (NES, 1993) Museum Theme
    https://www.youtube.com/watch?v=YjlYpDGQyjg ]

and, for the Commodore 64, loading music. Music played while the
game was loading, slowly, from tape.





[ scrub below to history ]

According to Wikipedia,
the first commercial digital synthesizer was Casio's VL-1
released in 1979.
It looks like a child's toy and sounds even worse.

[ The Casio Song (1979) ]

It was a period of rapid change.
In 1983, only a few years later,
Yamaha introduced the DX-7.
A keyboard sized digital FM synthesizer for
the price of a good studio piano.
The sound was to become famous in pop songs round the world.

But the first digital music
was being made earlier than this,
but not using commercially available synthesizers.

In the 1960s groups at MIT, IBM, and no doubt elsewhere,
were producing digitally synthesized music
on a variety of computers that would cost anywhere from
a very comfortable salary to the price a good office building.


## 3 Hardware

The distinctive sound of this era stems from the hardware used
to generate sounds.

The General Instrument AY-3-8910 _Programmable Sound Generator_
is typical of the era.

This chip has three tone generators
that all work the same way.
The generator has a clock input at a fixed frequency,
2 MHz is typical.
To output a tone that a human can hear, we divide the clock down,
by waiting N clock ticks and flipping the output.
The larger N is, the longer the period, the lower the frequency.

The AY-3-8910 has three tone generators and a noise generator
that uses a 15-bit linear feedback shift register to generate
wideband noise.
Each of these 4 channels has a 4-bit volume,
and they are mixed together to create the final output.

This gives us 3-voice polyphony plus a sort of drum or
percussion track using the noise generator.

This 3 tones plus noise seems to hit a sweet spot.
The chip is wildly popular,
used in a variety of machines and designs.
It's cloned and licensed, and inspires similar designs from
other manufacturers.

This particular chip, the AY-3-8910, also has
two 8-bit Input/Output ports which in consoles and home micros
are used to interface to printers or joysticks or other peripherals.
Making it a popular choice.

This chip was used in
the Intellivision, the Amstrad CPC,
the Mockingboard soundcard for the Apple II, the MSX standard,
and the ZX Spectrum 128K where, to distinguish the sound from
the older spectrum beeper, the music was called AY-tunes.

A Yamaha variant was used in the Atari ST.

Texas instruments had a competing part call the SN76489.
That too had a 3 tone plus noise design, and was used in the
Acorn BBC Micro, the Sega Master, was _one_ of the sound chips
in the Sega Genesis, and was used in the Neo Geo Pocket Color.

That's the off-the-shelf chips.

When rich flashy Atari were making the Atari 800 they were not
content with using an off-the-shelf part, they wanted something
more... custom.

Atari designers made the POKEY.
The name is short for Pot Keyboard and was a peripheral chip
responsible for reading the potentiometers on the paddle
controllers, scanning the keyboard, generating sound, and
possibly some other things.

It had 4 sound channels with somewhat coarse frequency selection,
but 2 channels could be combined into a single higher resolution channel.
Each channel had selectable volume, duty cycle, and noise distortion.

The POKEY was also used in Atari's arcade cabinets.

The MOS Technology SID, Sound Interface Device, was custom
designed for the Commodore 64.
3 channels only, but each channel had programmable waveform
including noise, volume envelope control, ring modulation,
oscillator sync, and the ouput was fed through a programmable
3-part filter.

The SID was clearly special.
Earning a fanbase amongst both musicians that programmed the chip,
and among the listeners of the tunes,
the SID-chip escaped the confines of 1980's videogames and
became a legitimate musical instrument.
The SID-chip itself got literally ripped out of the
microcomputers it was in and placed into a dedicated music synthesizer.

Later, ambitious programmers would make all of the sound chips do
something they weren't supposed to be able to do:

Pulse Code Modulation.

aka PCM for short.
PCM is the obvious way of encoding sampled sound and
is in universal use today.
Divide time into a fixed number of samples,
at each sample determine the level of output,
describe that level with a single number.
8-bit samples use an 8-bit numbers, 16-bit samples use a 16-bit
numbers.
Both the number of samples, and the bitwidth of the samples
affects quality and fidelity of the sound.
8-thousand samples per second and 8-bit numbers were judged
adequate for conveying human speech over the telephone system.
Hi Fi quality CD music uses 44,100 samples per second and 16-bit
numbers.

In terms of the 8-bit hardware of this era,
Pulse Code Modulation has a problem.
And the problem is that PCM requires a large of storage
which was expensive.
So even when PCM was possible it was only used in short bursts.


## Homage to YouTube

YouTube is incredible.
Almost every video game tune you can think of has been ripped by
someone and uploaded to YouTube for our enjoyment and
edification.

People curate and collect these into "Best Ofs" and theme based
OSTs.

If you want the BGM for the "Gall Force: Defense of Chaos",
a 1986 MSX game released in Japan and based on the 1980s Anime Gall
Force, you only have to search.

    [ Gall Force: Defense of Chaos (MSX, 1986) ]

This has been an incredible journey and would not have been
possible without these YouTube fans.

Thank you YouTubers.

## 6 Genre and Future

What is the purpose of Art?
What is the purpose of video game music?

Video Game Music has roughly the following forms:

Introduction and attract-mode.
Before the game proper starts, our attention and the computers
processing power, can be dedicated to a little musical
introduction.
This music can be singularly important for setting the scene for
a game and drawing you in emotionally.
The opening intro music is the first thing a player hears after
they buy a new game and plug it into their console.

Background Music is the music playing, in the background, while
the game is underway.
It sets the emotional tone.
It can be punchy, upbeat, racing along, threatening, calming,
inspirational, scary.
And it has to do all of these things while fitting in with
whatever the protagonist is doing in the game.
Background music has a beginning, but it doesn't really have an
end. Most games repeat it on a loop, and this loop has to be
catchy and compelling without being annoying.
No one wants to chisel their own ears off after listening to
background music for too long.

The remaining use of music in video games is to mark various
episodes or accomplishments.
Moments like restarting a save file, dying, levelling up,
finding a new item, reaching a safe spot, a boss battle,
and the final victory moments of a game.

In the victory scene of a game, it's typical to have the full
game credits roll, and this usually gives enough time for one or
two full length tunes


## Miscellaneous

A capella Duck Tales: https://www.youtube.com/watch?v=Nhy2aNEPshw

Alberto Jose Gonzalez reprises his game boy tune https://www.youtube.com/watch?annotation_id=annotation_363689&feature=iv&src_vid=UnzHTkRLcE4&v=Y6B5CRwbthc

8-bit cover of an entire NiN album
https://en.wikipedia.org/wiki/Pretty_Eight_Machine_(album)

## Sound FX

The same sound hardware is usually doing duty for music as well
as game Sound FX.
Meaning that during gameplay music is limited.

We get the richest music experiences on this sort of hardware in
title and intertitle sequences.

Until streaming came along (PSX?).


## Stereo

Is there any?

On SMS? And Game Boy (Color?)?


## Disney

There are quite a lot of decent NES tunes by Disney

The Lion King seems quite weak.

Beauty and the Beast has a cheesy 8-bit version of the hit
single that provided the radio-friendly fuel to boost
then unknown Celine Dion's career.
https://www.youtube.com/watch?v=no9vhOsZ2XQ&index=3&list=PLF3553C6C107029BC

The Little Mermaid has quite a chirpy (and good) cover of "Under
the Sea".
https://www.youtube.com/watch?v=4Yopn26rFXw&index=2&list=PL7E195B713C46CBAB

Aladdin has a competent title
https://www.youtube.com/watch?v=zNG8aA-L-IA&list=PLtQCUAILIEZ0sx_PKD44VEvxkeXMghmlu&index=14

The Sega Master System version of Aladdin is good. https://www.youtube.com/watch?v=kKc3M72fJM0
(watch out for 60Hz versions!)

The list of Disney properties is quite long.
Before researching this I had never heard of Chip 'n Dale.
Didn't know it was a Disney property.
Didn't know there was a NES game called "Chip 'n Dale Rescue
Rangers".
Didn't know said game sold 1.2 million copies.
Which is a lot.
Didn't know said game has many ridiculously good tunes
laid down by Capcom's Harumi Fujita.

As many seem to have noticed, Zone J seems to be the best.
https://www.youtube.com/watch?v=u0EnL4M1jjE&index=8&list=PLD7A491D8B8443456

Duck Tales (NES) Moon Stage is a banger too
https://www.youtube.com/watch?v=B0evYaCV5QA&index=4&list=PL0FE0D134995B8F2B

Launchpad Copter is also respectable.

TaleSpin (NES) has some good tunes.
Nothing as good as Chip 'n Dale or Duck Tales, but
Bonus Stage is quite strong.

Who Framed Roger Rabbit (NES) Continue theme.
This is a really good example of the thing these digital chips do
best. Drum and Bass.
https://www.youtube.com/watch?v=CovbF2pvzyM&list=PLC107C3246F26C0D5&index=6

Adventures in the Magic Kingdom (NES) Train Game.
A jaunty honky tonk tune descends into chaos.
https://www.youtube.com/watch?v=XqIoMDLlaQc&index=10&list=PL248C24460A761F81


Jungle Book (NES) also good.
https://www.youtube.com/watch?v=kK8DJIoXQCg&list=PL576F1DC3E76FFA12&index=4
though possibly exceeded by the Sega Master System version.


