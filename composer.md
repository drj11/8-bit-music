## From http://www.cpcwiki.eu/index.php/Chip_Tune

Chris hülsbeck
Martin Galway
Jochen Hippel

# Steve Turner

Quazatron


## Tim Follin

A legend.


## Jeroen Tel

Alien 3 and so on.

Is on YouTube: https://www.youtube.com/user/freejaytea

15 in 1988? and a gamer and a programmer


## Rob Hubbard

A legend.

## Ben Daglish

A legend. Died 2018.

## Minako Hamano

Links's Awakening, BGM Composer [http://nintendo.wikia.com/wiki/Minako_Hamano]

Super Metroid (SNES 1994), Music Composer [http://nintendo.wikia.com/wiki/Minako_Hamano]

is the Super Metroid credit for BGM? Theme might be Kenji Yamamoto

Metroid Fusion (GBA, 2002)

Dr Kawashima's Brain Training: How Old Is Your Brain? (DS, 2005) Music Composition

Metroid Prime 3: Corruption (Wii, 2007) Music

CDs https://vgmdb.net/artist/635

IMDB: https://www.imdb.com/name/nm0357344/?ref_=ttfc_fc_cr4


## Kozue Ishikawa

Link's Awakening

http://nintendo.wikia.com/wiki/Kozue_Ishikawa


## Kenji Yamamoto

Super Metroid
Famicom Wars


## Kazumi Totaka

Super Mario Land 2: Six Golden Coins (GB, 1992)

Tetris and Dr Mario (SNES, 1994)
