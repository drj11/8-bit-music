# video game music hardware


## AY-3-8910

General Instruments AY-3-8910 aka YM2149F.

Amstrad CPC, ZX Spectrum 128K, Intellivision.
Atari ST (variant). MSX (often a variant).

Mockingboard accessory card for Apple II.
https://en.wikipedia.org/wiki/Mockingboard
See Ultima IV intro: https://www.youtube.com/watch?v=2LlicAXhkX4


## SN76489

A number of machines shared the popular Texas Instruments SN76489 chip.

The BBC Micro and the Sega Master and Genesis systems.

4 channels: 3 square wave, plus 1 noise ("white", "pink", "periodic").


## Atari 800

POKEY

4 channels with selectable noise/tone mix.
Channel frequency is course (8-bit), but 2-channels can be
paired to create a 16-bit channel.
This enables a 3-channel (one high-res, two low),
and a 2-channel mode.

Also used in some arcade games. Clever.

Extremely capable sound hardware for the 1979 release date.
Not until 1982's C64 SID chip did anything comparable come onto
the market.
And even then, SID isn't clearly superior,
it just has different capabilities.


## NES

Ricoh RP2A03.
5 channel total.
2 square wave with variable duty and hardware sweep.
1 triangle wave, no volume modulation.
1 noise channel ("white" or "periodic").
1 DPCM channel. Fairly crude samples

On Famicom also had audio mix-in
meaning that certain cartridges could enhance the sound.

## Commodore 64

SID Chip.
3 channels.
Each channel's waveform can be either square, triangle, sawtooth, or noise.
Each channel has separate ADSR control.
3 ring modulators and oscillator sync.
3-stage adjustable filter.
Audio mix-in.
Glitch enables CPU intensive PCM samples.

## Game Boy

4 voices.
2 pulse wave.
1 4-bit PCM.
1 noise channel.
Audio mix-in from cartridge.

Limited stereo: each channel can be output to either left- or
right-channel or both. Separate left- and right-channel volumes.

## SNES

Dedicated Sound Processor. 8-bit CPU + 16-DSP.
8 voice 16-bit samples, plus reverb.

## Spectrum

1-bit beeper. Terrible.

But the Spectrum 128K had the 3+1 channel AY-3-8910.
Superficially, this makes it like the Amstrad CPC, but it _also_
has the beeper.

