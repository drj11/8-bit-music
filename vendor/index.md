littlefugue.mp3

  Bach's Little Fugue arranged for PDP-1, Dan Smith.
  http://www.dpbsmith.com/pdp1music/index.html

Code Name - Viper (NES) Music - Area Clear Points-mQT-yay3K1w.opus

  Code Name: Viper - Area Clear https://www.youtube.com/watch?v=mQT-yay3K1w

RoboCop Gameboy Title Theme (best quality)-wGIKnn-COS4.m4a

Kirby's Adventure (NES) Music - Museum Theme-YjlYpDGQyjg.opus

  https://www.youtube.com/watch?v=YjlYpDGQyjg

Metroid II - Return Of Samus Music - Title Theme-453temu5BIg.m4a

  https://www.youtube.com/watch?v=453temu5BIg

The Great Giana Sisters - Menu Theme [C64]-Mafip6Am99c.opus

  Great Giana Sisters (C64) from https://www.youtube.com/watch?v=Mafip6Am99c

Zybex (Atari XL_XE)-Nlpg2OYkhMA.opus

  Zybex (Atari XL) from https://www.youtube.com/watch?v=Nlpg2OYkhMA

Super Mario Bros. - Castle Clear Fanfare-DBOb7H4vzTM.opus

  https://www.youtube.com/watch?v=DBOb7H4vzTM

BATMAN RETURN OF THE JOKER (GB) - Track 11 - Game Over-2Pk9JBSrEgM.m4a

  https://www.youtube.com/watch?v=2Pk9JBSrEgMc
